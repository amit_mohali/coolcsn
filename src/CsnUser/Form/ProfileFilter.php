<?php
namespace CsnUser\Form;

use Zend\InputFilter\InputFilter;

class ProfileFilter extends InputFilter
{
    public $pass_length_min = 6;
    public $pass_length_max = 100;

    public function __construct($sm)
    {
    	 $this->add(array(
            'name'     => 'DisplayName',
            'required' => true,
            'filters'  => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name'    => 'StringLength',
                    'options' => array(
                        'encoding' => 'UTF-8',
                       	'min'=>3,
                        'max'      => 100,
                    ),
                ),
            ),
        ));

      
        $this->add(array(
            'name'       => 'Email',
            'required'   => true,
            'validators' => array(
                array(
                    'name' => 'EmailAddress'
                ),
            ),
        ));
    	
        $this->add(array(
            'name'       => 'LandingPage',
            'required'   => true,
            )
        );
            
    	 $this->add(array(
            'name'       => 'DateFormat',
            'required'   => true,
            'validators' => array(
                array(
		            'name' => 'InArray',
		            'options' => array(
						'haystack' => array('d/m/Y','m/d/Y','d-M-y'),
						'messages' => array(
						\Zend\Validator\InArray::NOT_IN_ARRAY => 'Please select date format'
						),
					),
		        ),
            ),
        ));
        
       
        
    }
    
    public function validateCurrentPassword()
    {
    	 $validator=array(
            'name'     => 'currentPassword',
            'required' => true,
            'filters'  => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name' =>'NotEmpty',
                    'options' => array(
                        'messages' => array(
                            'isEmpty' => 'Please enter your current password'
                        ),
                    ),
                )
            ),
        );
		$v1 = $this->getFactory()->createInput( $validator );
	    $this->add($v1);
	    return $this;
    }
    
    public function validateNewPassword()
    {
    	$validator1=array(
            'name'     => 'newPassword',
            'required' => true,
            'filters'  => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name' =>'NotEmpty',
                    'options' => array(
                        'messages' => array(
                            'isEmpty' => 'Please enter new password'
                        ),
                    ),
                    'break_chain_on_failure' => true
                ),
                 array(
                    'name'    => 'StringLength',
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'min'      => $this->pass_length_min,
                        'max'      => $this->pass_length_max,
                        'messages' => array(
                            'stringLengthTooShort' => 'Password minimum length is '.$this->pass_length_min.' chars',
                            'stringLengthTooLong' => 'Password maximum length is '.$this->pass_length_min.' chars'
                        ),
                    ),
                    'break_chain_on_failure' => true
                ),
            )
         );   
 		$v1 = $this->getFactory()->createInput( $validator1 );
	    $this->add($v1);
        $validator2=array(
            'name'     => 'newPasswordConfirm',
            'required' => true,
            'filters'  => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name' =>'NotEmpty',
                    'options' => array(
                        'messages' => array(
                            'isEmpty' => 'Please enter confirm new password'
                        ),
                    ),
                    'break_chain_on_failure' => true
                ),
                array(
                    'name'    => 'StringLength',
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'min'      => $this->pass_length_min,
                        'max'      => $this->pass_length_max,
                        'messages' => array(
                            'stringLengthTooShort' => 'Password minimum length is '.$this->pass_length_min.' chars',
                            'stringLengthTooLong' => 'Password maximum length is '.$this->pass_length_min.' chars'
                        ),
                    ),
                    'break_chain_on_failure' => true
                ),
                array(
                    'name'    => 'Identical',
                    'options' => array(
                        'token' => 'newPassword',
                    )
                ),
            ),
		);
		$v2 = $this->getFactory()->createInput( $validator2 );
	    $this->add($v2);    
	    return $this;   
    }
}
