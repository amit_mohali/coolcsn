<?php
namespace CsnUser\Form;

use Zend\Form\Form;

class ProfileForm extends Form
{
    protected $em;
	protected $locale;
    public function __construct($locals,$em)
    {
    	$this->em=$em;
    	$this->locale=$locals;
        parent::__construct('user');
        $this->setAttribute('method', 'post');
        $this->setAttribute('role', 'form');
        $this->setAttribute('class', 'form-horizontal');
        $this->setAttribute('enctype', 'multipart/form-data');
        
        $this->add(array(
            'name' => 'DisplayName',
            'attributes' => array(
                'type'  => 'text',
                'placeholder' =>'Display Name',
                'class'=>""
            ),
            'options' => array(
                'label' => ' ',
            ),
        ));

       $this->add(array(
            'name' => 'Email',
            'attributes' => array(
                'type'  => 'email',
                'placeholder' =>'Email',
                'class'=>""
            ),
            'options' => array(
                'label' => ' ',
            ),
        ));

        $this->add(array(
            'name' => 'currentPassword',
            'attributes' => array(
                'type'  => 'password',
                'placeholder' =>'Current password',
            ),
            'options' => array(
                'label' => ' ',
            ),
        ));

        $this->add(array(
            'name' => 'newPassword',
            'attributes' => array(
                'type'  => 'password',
                'placeholder' =>'New password',
            ),
            'options' => array(
                'label' => ' ',
            ),
        ));

        $this->add(array(
            'name' => 'newPasswordConfirm',
            'attributes' => array(
                'type'  => 'password',
                'placeholder' =>'Confirm new password',
            ),
            'options' => array(
                'label' => ' ',
            ),
        ));
		
        $this->add(array(
             'type' => 'Zend\Form\Element\Select',
             'name' => 'DateFormat',
             'options' => array(
                     'label' => false,
                     'value_options' =>$locals['DateFormat']
             ),
             'attributes' => array(
				'value' => 0 
			)
     	));
     	
     	 $this->add(array(
             'type' => 'Zend\Form\Element\Select',
             'name' => 'LandingPage',
             'options' => array(
                     'label' => false,
                     'value_options' =>$locals['LandingPage']
             ),
             'attributes' => array(
				'value' => '' 
			)
     	));
     
     	 $this->add(array(
            'name' => 'ProfilePic',
            'attributes' => array(
                'type'  => 'file',
                'class'=>"",
                'id'=>'ProfilePic'
            ),
            'options' => array(
               'label' => 'Choose an image',
            ),
        ));
        
	     $this->add(array(
	      'name' => 'security',
	      'type' => 'Zend\Form\Element\Csrf'
	    ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Go',
                'class' => 'btn btn-success btn-lg',
            ),
        ));
    }
}
