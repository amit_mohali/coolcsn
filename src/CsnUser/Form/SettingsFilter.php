<?php
namespace CsnUser\Form;

use Zend\InputFilter\InputFilter;

class SettingsFilter extends InputFilter
{
    public function __construct($sm)
    {
    	
        $this->add(array(
            'name'       => 'CurrencyCode',
            'required'   => true,
            )
        );
        
    }
}
