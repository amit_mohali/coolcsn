<?php
namespace CsnUser\Form;

use Zend\Form\Form;

class SettingsForm extends Form
{
    protected $em;
	protected $locale;
    public function __construct($locals,$em)
    {
    	$this->em=$em;
    	$this->locale=$locals;
        parent::__construct('user');
        $this->setAttribute('method', 'post');
        $this->setAttribute('role', 'form');
        $this->setAttribute('class', 'form-horizontal');

     	$this->add(array(
             'type' => 'DoctrineModule\Form\Element\ObjectSelect',
             'name' => 'CurrencyCode',
             'options' => array(
                     'label' => false,
                     'empty_option' => 'Select Currency',
                     'object_manager' => $this->em,
		             'target_class'   => 'Application\Entity\Currency',
		             'property'       => 'CurrencyName',
		             'find_method'       => array(
		                'name' => 'findBy',
		                'params' => array(
		                    'criteria' => array(),
		                    'orderBy' => array('CurrencyName' => 'asc'),
		                ),
		            ),
             ),
             'attributes' => array(
				'value' => '',
				'id'=>'CurrencyCode'
				//'class'=>"col-xs-10 col-sm-5  no-padding-right" 
			)
     	));
     
        
	     $this->add(array(
	      'name' => 'security',
	      'type' => 'Zend\Form\Element\Csrf'
	    ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Go',
                'class' => 'btn btn-success btn-lg',
            ),
        ));
    }
}
