<?php
namespace CsnUser;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\ServiceManager\ServiceManager;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;
//use Zend\Mail\Transport\Smtp;
//use Zend\Mail\Transport\SmtpOptions;

class Module implements AutoloaderProviderInterface, ConfigProviderInterface, ServiceProviderInterface
{
	protected $public_resource_actions=array('login','logout','home','forgotten-password','CronJob','API', 'pomap', 'tracking', 'dashboard-drilldown', 'dashboards/teamoverviewresult');
	public function onBootstrap(MvcEvent $e) {
		$app=$e->getApplication();
    	$em  = $app->getEventManager();
        $sm  = $app->getServiceManager();
        $auth = $sm->get('Zend\Authentication\AuthenticationService');
        $resources=$this->public_resource_actions;
        $em->attach(MvcEvent::EVENT_ROUTE, function($e) use ($auth,$resources) {
            $match = $e->getRouteMatch();
            $controller = $match->getParam('controller');
            $namespace = $match->getParam('__NAMESPACE__');
        	$action=$match->getParam('action');
            $name = $match->getMatchedRouteName();
           	$response = $e->getResponse();
           	$request = $e->getRequest();
            $router   = $e->getRouter();
            
            // check for API module
            $namespaceArray=explode("\\",$namespace);
            if(!empty($namespaceArray[0]) && ($namespaceArray[0]=='API' or $namespaceArray[0]=='CronJob')){
            	$name=$namespaceArray[0];
            }
            
            
            // break for default in action 
            $default_array=explode("/",$name);
            if(!empty($default_array[1]) && $default_array[1]=='default') {
            	$name=$default_array[0];
            }
            
			// login needed to access protected modules
			if(!in_array($name,$resources)) {
				if (!$auth->hasIdentity()) {
	        		$url      = $router->assemble(array(), array(
		                'name' => 'home'
		            ));
		            $response->getHeaders()->addHeaderLine('Location', $url);
		            $response->setStatusCode(302);

		            return $response;

		        } else {

                    // check policy password change //
                    $container = new \Zend\Session\Container('accounts');
                    if(!empty($container->force_pwd_change) && !$request->isXmlHttpRequest()) {
                        if(!in_array($_SERVER['REQUEST_URI'], array('/profile', '/profile/'))) {
                            $url = $router->assemble(array(), array(
                                'name' => 'profile'
                            ));
                            $response->getHeaders()->addHeaderLine('Location', $url);
                            $response->setStatusCode(302);

                            return $response;
                        }
                    }

					// redirect loggedin user, when try to access administrator area
					$groupID=$auth->getIdentity()->getGroupID()->getID();
					$landingPage='dashboard';
					if($auth->getIdentity()->getLandingPage()!="") {
						$landingPage=$auth->getIdentity()->getLandingPage();
					}

		        	if($controller=="Administrator\Controller\Index" and $groupID!=1) {
		        		$url      = $router->assemble(array(), array(
		                'name' => $landingPage
			            ));
			            $response->getHeaders()->addHeaderLine('Location', $url);
			            $response->setStatusCode(302);
			            return $response;
					} else {
						///////// check if loggedin user has access to particular module //////////////
						if($groupID!=1 && !in_array($controller,array("ajaxUtil",'docAjaxUtil','bsAjaxUtil','mspmtAjaxUtil','mpoAjaxUtil','miAjaxUtil','dmAjaxUtil','sscsAjaxUtil','configAjaxUtil','CsnUser\Controller\Index','Util','truckingAjaxUtil', 'ecomAjaxUtil', 'wmsAjaxUtil', 'usTruckingAjaxUtil', 'pickAjaxUtil', 'sleAjaxUtil', 'scsAjaxUtil', 'contactsAjaxUtil','shipmentAjaxUtil','dashboardsAjaxUtil', 'adminAjaxUtil','Application\Controller\Notifications')))	{
							$loggedinUserAccessModules=$auth->getIdentity()->getGroupID()->getGroupModules();
							$modules=array('Application');
							$routes=array('dashboard', 'managesaseu/saseuresult','managesaseu/tariff','managesaseu/routes');
							foreach($loggedinUserAccessModules as $m) {
								$modules[]=$m->getModuleName();
								$r=$m->getRoute();
								if(!empty($r)) {
									$rr=explode("@",$r);
									$routes=array_merge($routes,$rr);
								}
							}
							
							// set all routes to view
							$e->getViewModel()->setVariable('routes',$routes);
							$namespace=explode("\\",$namespace);
							$cmname=$namespace[0];
							$dashed_name=$name;
							$name=str_replace(array("default",'-'),array('',''),$name);

							if(in_array($cmname,$modules)) {
								if(!in_array($name,$routes) and !in_array($name."/",$routes) and !in_array($dashed_name, $routes) and !in_array($name.'/'.$action, $routes)) {
									$url      = $router->assemble(array(), array(
					                'name' => $landingPage
						            ));
						            $response->getHeaders()->addHeaderLine('Location', $url);
						            $response->setStatusCode(302);
						            return $response;
								}
							} else { 
								$url      = $router->assemble(array(), array(
				                'name' => $landingPage
					            ));
					            $response->getHeaders()->addHeaderLine('Location', $url);
					            $response->setStatusCode(302);
					            return $response;
							}
						}
						////////////////// End here ///////////////////
					}
		        }
			}
            
        }, -100);
    }
	
	public function getConfig() {
        return include __DIR__ . '/../../config/module.config.php';
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/../../src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig() {
        return array(
//-			'aliases' => array( // !!! aliases not alias
//-				'Zend\Authentication\AuthenticationService' => 'doctrine_authenticationservice', // aliases can be overwriten
//-			),
            'factories' => array(
                // taken from DoctrineModule on GitHub
                // Please note that I am using here a Zend\Authentication\AuthenticationService name, but it can be anything else
                // However, using the name Zend\Authentication\AuthenticationService will allow it to be recognised by the ZF2 view helper.
                // the configuration of doctrine.authenticationservice.orm_default is in module.config.php
                'Zend\Authentication\AuthenticationService' => function($serviceManager) {
//-                 'doctrine_authenticationservice'  => function($serviceManager) {
                    // If you are using DoctrineORMModule:
                    return $serviceManager->get('doctrine.authenticationservice.orm_default');

                    // If you are using DoctrineODMModule:
                    //- return $serviceManager->get('doctrine.authenticationservice.odm_default');
                },

                /*// Add this for SMTP transport
                'mail.transport' => function (ServiceManager $serviceManager) {
                    $config = $serviceManager->get('Config');
                    $transport = new Smtp();
                    $transport->setOptions(new SmtpOptions($config['mail']['transport']['options']));

                    return $transport;
                },
*/
                'csnuser_module_options' => function ($sm) {
                    $config = $sm->get('Config');

                    return new Options\ModuleOptions(isset($config['csnuser']) ? $config['csnuser'] : array());
                },
            )
        );
    }
}
