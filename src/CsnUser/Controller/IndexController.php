<?php
namespace CsnUser\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Application\Entity\User; 
use Application\Entity\UserStats; 
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;
use CsnUser\Form\LoginForm;
use CsnUser\Form\LoginFilter;

use CsnUser\Form\ProfileForm;
use CsnUser\Form\ProfileFilter;

use CsnUser\Form\SettingsForm;
use CsnUser\Form\SettingsFilter;

use CsnUser\Options\ModuleOptions;
use Zend\Crypt\BlockCipher;

use Zend\Session\Container;

class IndexController extends AbstractActionController
{
    /**
     * @var ModuleOptions
     */
    protected $options;

    /**
     * @var Doctrine\ORM\EntityManager
     */
    protected $em;

    /**
     * Index action
     *
     * The method show to users they are guests
     *
     * @return Zend\View\Model\ViewModelarray navigation menu
     */
    public function indexAction()
    {
        return new ViewModel(array('navMenu' => $this->getOptions()->getNavMenu()));
    }

    /**
     * Log in action
     *
     * The method uses Doctrine Entity Manager to authenticate the input data
     *
     * @return Zend\View\Model\ViewModel|array login form|array messages|array navigation menu
     */
    public function loginAction()
    {
        if ($user = $this->identity()) {
            return $this->redirect('/dashboard');
        }
        $form = new LoginForm();
        $form->get('submit')->setValue('Log in');
        $messages = null;
        $error=null;
		$sm = $this->getServicelocator();
        $request = $this->getRequest();
        $i=1; $acntdata=array();
        // Process ajax login request
        if ($this->getRequest()->isXmlHttpRequest())
        {
        	$response=array("title"=>"Authentication Error","content"=>'',"status"=>0);
	        if ($request->isPost()) 
	        {
	            $form->setInputFilter(new LoginFilter($sm));
	            $form->setData($request->getPost());
	            if ($form->isValid()) 
	            {
	                $data = $form->getData();
	               
	                $authService = $sm->get('Zend\Authentication\AuthenticationService');
	                $adapter = $authService->getAdapter();
	                $usernameOrEmail = trim($data['usernameOrEmail']);
	              
	                // find user details
					$user = $this->getEntityManager()->getRepository('Application\Entity\User')->getUser($usernameOrEmail);

					// if user data found
					if(!empty($user)) 
					{ 	// For check last login date not more than 90 days
						$userLastLog =  $this->getEntityManager()->getRepository('Application\Entity\User')->getUserLastLogins($user['ID']);
						
						$last_login = $userLastLog['LastLogin']; 
						$total_last_login=$last_login + (90*24*3600);
						$currentTime=time();

						// check account status
						if($user['Status'])
						{
							if($total_last_login >= $currentTime)
							{	
								// decrypt password
								$config = $sm->get('Config');
								$blockCipherConfig = $config['blockCipherConfig'];
								$blockCipherIns = BlockCipher::factory('mcrypt', array('algo' => $blockCipherConfig['algo']));
								$blockCipherIns->setKey($blockCipherConfig['key']);
								$pwd=$blockCipherIns->decrypt($user['Password']);	
								  
								
								// validate password
								if($pwd==$data['Password'])
								{
									$authService->getStorage()->write((object)$user);
									$time =(30 * 24* 60 * 60); 
				
									if (!empty($data['rememberme'])) {
										$sessionManager = new \Zend\Session\SessionManager();
										$sessionManager->rememberMe($time);
									}
									$response['status']=1;
									
									$landing_page='/dashboard';
									if($this->identity()->getGroupID()->getLandingPage()!="") {
										$landing_page="/".$this->identity()->getGroupID()->getLandingPage();
									}
									if($this->identity()->getLandingPage()!="") {
										$landing_page="/".$this->identity()->getLandingPage();
									}
									
									######## update user stats #########
									$ustats = $this->getEntityManager()->getRepository('Application\Entity\UserStats')->findBy(array('UserID' =>$this->identity()->getID()));
									$no_of_login=1;
									$time=time();
									if(empty($ustats[0]))
									{
										$usObj= new UserStats();
										$usObj->setUserID($this->identity()->getID());
										$usObj->setNoOfLogins(1);
										$usObj->setLastLogin($time);
									}
									else 
									{
										$usObj = $this->getEntityManager()->find('Application\Entity\UserStats', $ustats[0]->getID());
										$no_of_login= $usObj->getNoOfLogins() + 1 ;
										$usObj->setNoOfLogins($no_of_login);
										$usObj->setLastLogin($time);
									}
									$this->getEntityManager()->persist($usObj);
									$this->getEntityManager()->flush(); 
									/// end here
									
									//For user accout division
									$container = new Container('accounts');
									$userAccount = $this->getEntityManager()->find('Application\Entity\User', $user['ID']);
									foreach ($userAccount->getAccounts()->toArray() as $account) { 
										$division=$sm->get('qry-util')->getDivisions($account->getID());
										$acc=array(
											'ID'=>$account->getID(),
											'BTOfficeID'=>$account->getBTOfficeID(),
											'DB'=>$account->getODBCName(),
											'Control'=>$account->getControl(),
											'AccountNo'=>$account->getAccountNo(),
											'Division'=>$division
										);
										$acntdata[]=$acc;
									}
									$container->accountInfo = $acntdata;
									$_SESSION["isAdmin"] = false;
									//For user account DBs
                                    /*$getAllData=$sm->get('qry-util')->getUserAccountDBs($user['ID']);
                                    $dbData=array();
									foreach($getAllData as $userDB) { 
										$dbData[$userDB['Db']][]=$userDB['Acc'];
									}
									$container->accountInfoDB = $dbData;
                                    */

									// get access of all brance and account access
                                    $dbData=$branchDbs=array();
                                    $allData=$sm->get('qry-util')->getUserBranchAccessWithAccounts($user['ID']);
									
                                    foreach ($allData as $row) {
                                        if($row['Account']!="All") {
                                            $dbData[$row['Db']][]=$row['Account'];
                                        } else {
                                            $branchDbs[] = $row['Db'];
                                        }
                                    }
                                    $container->accountInfoDB = $dbData;
                                    $container->accountBranchAccessDbs = $branchDbs;

									$container->accountInfoDBAccounts = $sm->get('qry-util')->fetchIndirectAccounts($dbData);

									// apply policy if found //
                                    $companyObj = $this->identity()->getCompanyID();
                                    $policy_data = [];
                                    if($companyObj->getPolicyID()!==null) {
                                        $policy_data = $companyObj->getPolicyID();
                                        $password_expiry = $policy_data->getPasswordExpiry();
                                        $last_password_date = $this->identity()->getPasswordUpdatedOn();//->format('Y-m-d H:i');
                                        if($last_password_date!==null) {
                                            $now = date_create(date("Y-m-d H:i"));
                                            $diff = date_diff($now,$last_password_date);
                                            $days = (int) $diff->format("%a");
                                            if($days > $password_expiry) { // force password change //
                                                $container->force_pwd_change = 1;
                                                $landing_page = '/profile';
                                            }
                                        }
                                    }

									$response['content']=$landing_page;
								}
								else 
								{
									$response['content']="Your supplied credentials are not valid.";
								}
							}
							else 
							{
								$response['content']="Your account is disabled due to inactivity, Please contact your account manager to be reactivated.";
							}
						}
						else 
						{
							$response['content']="Your account has been suspended, Please contact administrator.";
						}
					}
					else 
					{
						$response['content']="No record found.";
					}
	            }
	            else 
	            {
	            	// do nothing
	            }
	        }
	        echo json_encode($response);
	        exit(0);
        }
        else 
        {
        	return $this->redirect()->toRoute('home');
        }
        return new ViewModel(array(
                                'error' => $error,
                                'form'	=> $form,
                                'messages' => $error,
                                'navMenu' => $this->getOptions()->getNavMenu()
                            ));
    }

    /**
     * Log out action
     *
     * The method destroys session for a logged user
     *
     * @return redirect to specific action
     */
    public function logoutAction()
    {
        $auth = $this->getServiceLocator()->get('Zend\Authentication\AuthenticationService');
		
        // @todo Set up the auth adapter, $authAdapter
        if (!$auth->hasIdentity()) { // if session expired 
            return $this->redirect()->toRoute($this->getOptions()->getLogoutRedirectRoute());
        }
    
        $identity = $auth->getIdentity();
        $user_id=$this->identity()->getId();
        $auth->clearIdentity();
        $sessionManager = new \Zend\Session\SessionManager();
        $sessionManager->forgetMe();
        //$container = new Container('searchFilter');
		session_destroy();
		
		// unset locked data by user
		$sm = $this->getServicelocator();
		$sm->get('qry-bs')->unsetLockedRow($user_id);
		$sm->get('qry-bs')->unsetImportLockedRow($user_id);
		
        return $this->redirect()->toRoute($this->getOptions()->getLogoutRedirectRoute());

    }
    

    /**
     *  general settings of user
     */
    public  function settingsAction()
    {
    	 $entityManager = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
         if ($user = $this->identity())
         {
        	$sm = $this->getServicelocator();
        	$locals=$sm->get('Config');
            $form = new SettingsForm($locals,$this->getEntityManager());
            $form->setHydrator(new DoctrineHydrator($entityManager,'Application\Entity\User'));
            $form->bind($user);
          
            if($user->getCurrencyCode()=="" or $user->getCurrencyCode()==null)
            {
            	$form->get('CurrencyCode')->setValue($this->identity()->getGroupID()->getCurrencyCode());
            }
            $request = $this->getRequest();
            $message = null;
            $data=array();
            
            if ($request->isPost()) 
            {
            	$data = $request->getPost();
            	$filter=new SettingsFilter($sm);
            	$form->setInputFilter($filter);
                $form->setData($data);
                if ($form->isValid()) 
                {
                	$user->setCurrencyCode($data->CurrencyCode);
                	$entityManager->persist($user);
                    $entityManager->flush();
                    
                    // update ratex
                    if(!empty($data['_urx']) && !empty($data['ratex']))
                    {
                    	$res=$sm->get('qry-util')->updateRateX($data['ratex'],$user->getID());
                    }
                    
                    // update email alerts 
                   $sm->get('qry-util')->updateUserEmailAlert($data['EmailAlertSettings'],$user->getID());
                    
                    $this->flashMessenger()->addMessage("Data has been saved successfuly.");
					$this->redirect()->toUrl('/settings');
                }
            }
            
            // email alerts
            $userEmailAlertData=$sm->get('qry-util')->getUserEmailAlertData($user->getID(),array('show'=>1));
            
          	return new ViewModel(array('form' => $form,'data'=>$data, 'message' => $message,'userEmailAlertData'=>$userEmailAlertData ));
        } else {
            return $this->redirect()->toRoute($this->getOptions()->getLogoutRedirectRoute());
        }
    }

	
	function userAlertsAction() {
		$id = (int)$this->getEvent()->getRouteMatch()->getParam('id');
		$page = (int)$this->getEvent()->getRouteMatch()->getParam('page');
		$entityManager = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
        if ($user = $this->identity()) {
		
			if(empty($id)) {
				$id=$user->getID();
			}
			
			// if admin doing changes then change user PO details 
			$isadmin=$user->getIsAdministrator();
			if($isadmin==1) {
				$user=$entityManager->getRepository('Application\Entity\User')->find($id);
			}
			
			$sm = $this->getServicelocator();
			$data=array();
													
			$request=$this->getRequest();			
			if($request->isPost()) {
				$data=$request->getPost()->toArray();
				if(empty($data['PODelay'])) {
					$data['PODelay']=0;
				}
				if(empty($data['POActual'])) {
					$data['POActual']=0;
					$data['alertevents']=array();
				}
				
				// save alerts 
				if($sm->get('qry-util')->saveUserAlert($data, $id)) {
					$this->flashMessenger()->addMessage("Data has been saved successfuly.");
					if($isadmin==1) {
						$this->redirect()->toUrl('/alerts/'.$id.'/'.$page);
					} else {	
						$this->redirect()->toUrl('/alerts');
					}
				}
				
			} else {
				$data['PODelay']=$user->getPODelay();			
				$data['POActual']=$user->getPOActual();	
				//$data['alerts']=$sm->get('qry-util')->getUserAlerts($id);
				$data['alertevents']=$sm->get('qry-util')->getUserAlertEvents($id);
			}
				
			return new ViewModel(
				array(
					'data'=>$data,
					'allalerts'=>array(), //$sm->get('qry-util')->getAllAlerts(),
					'allcps'=>$sm->get('qry-util')->getUserCriticalPaths($id),
					'isadmin'=>$isadmin,
					'page'=>$page
				)
			);
			
		} else {
            return $this->redirect()->toRoute($this->getOptions()->getLogoutRedirectRoute());
        }
	}
	
	/**
	 *  change profile data
	 */
    
	public function profileAction()
	{
   	    $entityManager = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
        if ($user = $this->identity()) {
        	$sm = $this->getServicelocator();
        	$locals=$sm->get('Config');
        	$modules=$this->identity()->getGroupID()->getGroupModules();
        	$dashboard=array(''=>'-Select-','dashboard'=>'Dashboard');
        	/**
        	 * Disabled it to that David can force the user to default page for some time.
        	 * if(count($modules) > 0) {
				foreach ($modules as $module) {
					$dashboard[strtolower($module->getModuleName())]=$locals['ModuleSettings'][$module->getModuleName()]['name'];			
				}
			}*/
        	$locals['LandingPage']=$dashboard;
        	
            $form = new ProfileForm($locals,$this->getEntityManager());
            $form->setHydrator(new DoctrineHydrator($entityManager,'Application\Entity\User'));
            $form->bind($user);
          
            if($user->getLandingPage()=="" or $user->getLandingPage()==null) {
            	$form->get('LandingPage')->setValue($this->identity()->getGroupID()->getLandingPage());
            }

            // policy data //
            $companyObj = $user->getCompanyID();
            $policy_data = [];
            if($companyObj->getPolicyID()!==null) {
                $policy_data = $companyObj->getPolicyID();
            }

            $container_acc = new Container('accounts');
            $force_change_password = 0;
            if(!empty($container_acc->force_pwd_change)) {
                $force_change_password = 1;
            }

            
            $email = $user->getEmail();
            $username = $user->getUsername();
           
            $request = $this->getRequest();
            $message = null;
            if ($request->isPost()) {

            	$data = $request->getPost();
            	$filter=new ProfileFilter($sm);

            	// set policy password streangth //
                if(!empty($policy_data)) {
                    $filter->pass_length_min = $policy_data->getPasswordStrength();
                }

            	$config = $sm->get('Config');
				$blockCipherConfig = $config['blockCipherConfig'];

                if(!empty($data->newPassword) or !empty($force_change_password)) {
					$filter->validateCurrentPassword();
            		$filter->validateNewPassword();            	
                }
                $form->setInputFilter($filter);
                $form->setData($data);
							
                if ($form->isValid()) {
	                
                	$isEmailUpdated=false;
            		$isPasswordUpdated=false;
            		
            		// validate email and change password
                	if($email!=$data->Email)
	            	{
	            		$find=$this->em->getRepository("Application\Entity\User")->createQueryBuilder('u')
					   ->where('u.Email  = :uemail')
					   ->setParameter('uemail', $data->Email)
					   ->getQuery()
					   ->getResult();
	            		if(count($find) > 0)
	            		{
	            			$isEmailUpdated=true;
	            			$form->get('Email')->setMessages(array('A matching record was found'));
	            		}
	            	}
				
                	if(!empty($data->newPassword))
                	{
						$blockCipherIns = BlockCipher::factory('mcrypt', array('algo' => $blockCipherConfig['algo']));
						$blockCipherIns->setKey($blockCipherConfig['key']);
						$pwd=$blockCipherIns->decrypt($user->getPassword());	
						if($pwd!=$data->currentPassword) {
							$isPasswordUpdated=true;
	            			$form->get('currentPassword')->setMessages(array('Old password does not match'));

						} else { // validate current password and check last used password
						    if($data->currentPassword==$data->newPassword) {
                                $isPasswordUpdated = true;
                                $form->get('newPassword')->setMessages(array("You can not use your current password as new password"));
                            } else {
                                if (!empty($policy_data)) { 
                                    $used_pwd = $policy_data->getLastPasswordCount();
                                    if(!empty($used_pwd)) {
                                        $nth_used_passwords = $sm->get('qry-util')->getLastUsedPassword($user->getID(), $used_pwd);
                                        foreach ($nth_used_passwords as $row) {
												$pwd_log = $blockCipherIns->decrypt($row['Password']);
                                            if ($data->newPassword == $pwd_log) {
                                                $isPasswordUpdated = true;
                                                $form->get('newPassword')->setMessages(array("You can not use last {$used_pwd} used password"));
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                	}

                	
                	if(!$isEmailUpdated && !$isPasswordUpdated) {
                        $user->setDisplayName($data->DisplayName);
                        $user->setDateFormat($data->DateFormat);
                        $user->setEmail($data->Email);
                        $user->setLandingPage($data->LandingPage);
                        $user->setDateModified(new \DateTime());
                        if (!empty($data->newPassword)) {
                            $blockCipherIns = BlockCipher::factory('mcrypt', array('algo' => $blockCipherConfig['algo']));
                            $blockCipherIns->setKey($blockCipherConfig['key']);
                            $user->setPassword($blockCipherIns->encrypt($data->newPassword));
                        }

                        // upload user pic
                        $profilepic_data = $this->params()->fromFiles('ProfilePic');
                        if (!empty($profilepic_data['name'])) {
                            $extn = explode(".", $profilepic_data['name']);
                            $extn = array_reverse($extn);
                            $extn = $extn[0];
                            $profilepic = substr($profilepic_data['name'], 0, strrpos($profilepic_data['name'], ".")) . time() . ".$extn";
                            move_uploaded_file($profilepic_data['tmp_name'], $_SERVER['DOCUMENT_ROOT'] . '/img/ppic/' . $profilepic);
                            @unlink($_SERVER['DOCUMENT_ROOT'] . '/img/ppic/' . $user->getProfilePic());
                            $user->setProfilePic($profilepic);
                        }

                        // set password log //
                        if (!empty($data->newPassword)) {							
							$blockCipherIns = BlockCipher::factory('mcrypt', array('algo' => $blockCipherConfig['algo']));
							$blockCipherIns->setKey($blockCipherConfig['key']);
							$new_password = $blockCipherIns->encrypt($data->newPassword);
							
                            $sm->get('qry-util')->setPasswordLog(array($user->getID(), $new_password, date("Y-m-d H:i")));
                            $container_acc->force_pwd_change = '';
                            $user->setPasswordUpdatedOn(new \DateTime());
                        }

                        $entityManager->persist($user);
                        $entityManager->flush();
                        $this->flashMessenger()->addMessage("Profile data has been saved successfuly.");

	                    ### reset forms data
	                    $container = new Container('searchFilter');
						$container->shipment='';
						$container->purchaseorder='';
						$container->managepo=''; 
						$container->saseu='';
						$container1 = new Container('postData');
						$container1->_data='';
						$container2 = new Container('reportData');
						$container2->_data='';

						// reset dates for manage po and shipment map and stats //
						$pomap_container = new Container('pomapFilters');
                        $pomap_container->from_date = '';
                        $pomap_container->to_date = '';

                        $shipmentmap_container = new Container('shipmentmapFilters');
                        $shipmentmap_container->from_date = '';
                        $shipmentmap_container->to_date = '';

                        $shipstats_container = new Container('shipStatsFilters');
                        $shipstats_container->from_date = '';
                        $shipstats_container->to_date = '';

                        $mpo_stats_container = new Container('mpoStatsFilters');
                        $mpo_stats_container->from_date = '';
                        $mpo_stats_container->to_date = '';

						$this->redirect()->toUrl('/profile');
                	}
                }
            }

            return array('form' => $form, 'Username' => $username, 'message' => $message, 'force_change_password'=>$force_change_password);

        } else {
            return $this->redirect()->toRoute($this->getOptions()->getLogoutRedirectRoute());
        }
	}
    
	/**
	 *  forgotten password   
	 */
     
	public function forgottenPasswordAction()
    {
    	$request = $this->getRequest();
    	$sm = $this->getServicelocator();
    	
        // Process ajax login request
        if ($this->getRequest()->isXmlHttpRequest())
        {
			$res=array('status'=>0,'msg'=>'');
        	if ($request->isPost()) {
                $entityManager = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
                $data = $request->getPost();
                $user = $entityManager->getRepository('Application\Entity\User')->findOneBy(array('Email' => $data->Email));
                if(!empty($user))
                {
                	$password=$this->generatePassword(8,1,1,1);
                	$config = $sm->get('Config');
					$blockCipherConfig = $config['blockCipherConfig'];
					$blockCipherIns = BlockCipher::factory('mcrypt', array('algo' => $blockCipherConfig['algo']));
					$blockCipherIns->setKey($blockCipherConfig['key']);
					$user->setPassword($blockCipherIns->encrypt($password));
					$entityManager->persist($user);
					$entityManager->flush();
					
					// send email to user
					$mail_user_data=array('name'=>$user->getDisplayName(),'password'=>$password);
					$msg=$this->forward()->dispatch('myEmail', array('action' => 'sendEmail',"params"=>array("to"=>$data->Email,"template"=>'forgotten-pwd',"userdata"=>$mail_user_data,"subject"=>"HiCloud - your account new password")));
					$res['status']=1;
					$res['msg']="Your password has been reset and sent to you, please check your inbox";
                }
                else 
                {
                	$res['msg']="Account does not exist.";
                }
			}
			echo json_encode($res);
			exit();
        }
        else 
        {
        	return $this->redirect()->toRoute('home');
        }
    }

	
    
    public function generatePassword($l = 8, $c = 0, $n = 0, $s = 0)
    {
             // get count of all required minimum special chars
             $count = $c + $n + $s;
             $out = '';
             // sanitize inputs; should be self-explanatory
             if (!is_int($l) || !is_int($c) || !is_int($n) || !is_int($s)) {
                      trigger_error('Argument(s) not an integer', E_USER_WARNING);

                      return false;
             } elseif ($l < 0 || $l > 20 || $c < 0 || $n < 0 || $s < 0) {
                      trigger_error('Argument(s) out of range', E_USER_WARNING);

                      return false;
             } elseif ($c > $l) {
                      trigger_error('Number of password capitals required exceeds password length', E_USER_WARNING);

                      return false;
             } elseif ($n > $l) {
                      trigger_error('Number of password numerals exceeds password length', E_USER_WARNING);

                      return false;
             } elseif ($s > $l) {
                      trigger_error('Number of password capitals exceeds password length', E_USER_WARNING);

                      return false;
             } elseif ($count > $l) {
                      trigger_error('Number of password special characters exceeds specified password length', E_USER_WARNING);

                      return false;
             }

             // all inputs clean, proceed to build password

             // change these strings if you want to include or exclude possible password characters
             $chars = "abcdefghijklmnopqrstuvwxyz";
             $caps = strtoupper($chars);
             $nums = "0123456789";
             $syms = "!@#$%^&*()-+?";

             // build the base password of all lower-case letters
             for ($i = 0; $i < $l; $i++) {
                      $out .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
             }

             // create arrays if special character(s) required
             if ($count) {
                      // split base password to array; create special chars array
                      $tmp1 = str_split($out);
                      $tmp2 = array();

                      // add required special character(s) to second array
                      for ($i = 0; $i < $c; $i++) {
                               array_push($tmp2, substr($caps, mt_rand(0, strlen($caps) - 1), 1));
                      }
                      for ($i = 0; $i < $n; $i++) {
                               array_push($tmp2, substr($nums, mt_rand(0, strlen($nums) - 1), 1));
                      }
                     /* for ($i = 0; $i < $s; $i++) {
                               array_push($tmp2, substr($syms, mt_rand(0, strlen($syms) - 1), 1));
                      }*/

                      // hack off a chunk of the base password array that's as big as the special chars array
                      $tmp1 = array_slice($tmp1, 0, $l - $count);
                      // merge special character(s) array with base password array
                      $tmp1 = array_merge($tmp1, $tmp2);
                      // mix the characters up
                      shuffle($tmp1);
                      // convert to string for output
                      $out = implode('', $tmp1);
             }

             return $out;
    }
    
    
    /**
     * get entityManager
     *
     * @return EntityManager
     */
    public function getEntityManager()
    {
        if (null === $this->em) {
            $this->em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
        }

        return $this->em;
    }

     /**
     * set options
     *
     * @return IndexController
     */
    public function setOptions($options)
    {
        $this->options = $options;

        return $this;
    }

    /**
     * get options
     *
     * @return ModuleOptions
     */
    public function getOptions()
    {
        if (!$this->options instanceof ModuleOptions) {
            $this->setOptions($this->getServiceLocator()->get('csnuser_module_options'));
        }

        return $this->options;
    }
}
